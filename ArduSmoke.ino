/*
  Blink-Detect
 Turns on an IR LED on for one second, then off for one second, repeatedly.
 
 This is detected by a second IR Photoresistor
 
 Based on configuration here: http://www.reconnsworld.com/ir_ultrasonic_basicirdetectemit.html
 */

// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int EMITTER = 11;
int DETECTOR = 13;
int DETECTORREAD = A0;
int detectorValue = 0;
int POWER = 5; // Power Pin on the Arduino

// Temperature
int tempPin = A3;
float tempValue = 0.0;
int powerStatus = LOW;

// the setup routine runs once when you press reset:
void setup() {
  
  Serial1.begin(9600);
  while (!Serial1) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // initialize the digital pin as an output.
  pinMode(EMITTER, OUTPUT);     
  pinMode(DETECTOR, OUTPUT);
  pinMode(POWER, OUTPUT);

  digitalWrite(DETECTOR, HIGH);   // turn the LED on (HIGH is the voltage level)
  
  Serial1.println("Booted");
}

// the loop routine runs over and over again forever:
void loop() {
  delay(100);
  float tempSum = 0.0;
  for( int i=0; i<8; i++){
    tempSum += analogRead(tempPin);
    delay(10);
  }
  tempValue = tempSum * 5.0 * 100.0 / 8.0 / 1024.0;
  //Serial1.print("Temp: ");
  Serial1.print(tempValue);
  Serial1.print(", ");
  delay(100);

  digitalWrite(EMITTER, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);               // wait for a second

  // Read Detector
  detectorValue = analogRead(DETECTORREAD);
  delay(100);               // wait for a second
  //Serial1.print("Light High: ");
  Serial1.print(detectorValue);
  Serial1.print(", ");

  digitalWrite(EMITTER, LOW);    // turn the LED off by making the voltage LOW
  delay(100);               // wait for a second

  // Read Detector
  detectorValue = analogRead(DETECTORREAD);
  delay(100);               // wait for a second
  //Serial1.print("Light Low: ");
  Serial1.print(detectorValue);
  Serial1.print(", ");
  
  // Set the heat on or off
  if (Serial1.available() > 0)
  {
    char inByte = Serial1.read();
    switch(inByte){
    case 't': //toggle power
      if (powerStatus == LOW){ powerStatus = HIGH; }
      else { powerStatus = LOW; }
      digitalWrite(POWER, powerStatus);
    }
  }
  
  Serial1.print(powerStatus);
  Serial1.println("");
}
