import serial
import datetime
import curses

addr  = '/dev/ttyUSB0'#'/dev/ttyACM1'
baud  = 9600
fname = 'accel.dat'
fmode = 'a'
reps  = 100

stdscr = curses.initscr()
stdscr.nodelay(True)
inkey = None

print 'Connection Test'
s = serial.Serial(addr,baud)
print s.readline()
s.close()

with serial.Serial(addr,baud) as port, open(fname,fmode) as outf:    
    while inkey != ord(' '):
        
        l = port.readline()
        outf.write(str(datetime.datetime.now()))
        outf.write(', ')
        outf.write(l)
        outf.flush()

        inkey = stdscr.getch()
        
        if inkey == ord('t'):
            port.write('t')
    
    port.close()
